{{{
  "title" : "Virt-Manager’s Volume Storage (adding recursive-ness)",
  "tags" : [ "libvirt", "linux", "qemu", "volume manager" ],
  "category" : "Scripts",
  "date" : "10-11-2011",
  "description" : "Managing storage volumes with virt-manager"
}}}

Virt-Manager (a gui to the libvirt) is a management software that allows you to install and virtualize many different OS’s on the fly. One of the cool features would be its ability to manage your volumes as well. These volumes can be anything from raw harddisk images (.raw, .img, .cow, etc..) to the actually installation medium (cd/dvd iso’s). I found this to be awesome until it met my OCD.

(The problem) On my system, cd/dvd iso’s are alphabetized and arranged by folder (./a/arch-linux-iso.iso – ./f/fedora/.. – etc ..). However, virt-manager does not like to recursively scan this folder to update with all the media it contains. (The solution) – So of course this made me think (1) “How can I hack this or beat the system” (2) “I feel like coding”

The following script will recursively list all the cd/dvd “iso” and “img” files, then create (place) symbolic links (pointers) to these files in one folder that virt-manager can handle.

**Example file system**

<pre>
/data/path/of/iso/folder
/data/path/of/iso/a..
/data/path/of/iso/a/arch-linux-iso.iso
/data/path/of/iso/b..
/data/path/of/iso/c..
etc..
</pre>

The script is placed in the “/data/path/of/iso/folder” – Once in the root of your iso folder, the “rm” command will remove any currently existing symbolic links. It will then transverse up and out to “/data/path/of/iso/” – The command “tree” is used to list all files and folders recursively below the current folder. The flags -f and -i are you used to make this pretty. The first command grep is used to remove the all instances of “/data/path/of/iso/folder” (the place were all the pointers/symbolic links are going to be). The second grep command it used to separate the files from folders and put the full path of the files in a .txt file. Finally, the while function loops through that .txt file creating symbolic links for all the gathered paths to your iso/images, placing them in the current directory (you did put this script in the “/data/path/of/iso/folder” path right!? )

Here is the code:

<pre>
#!/bin/bash
#
rm ./*.iso ./*.img
#
tree -if ../ | grep -v "folder" | grep -i ".\.iso" > ./update-pool.txt
tree -if ../ | grep -v "folder" | grep -i ".\.img" >> ./update-pool.txt
#
while read line;
do
filename=$(echo $line | xargs basename)
ln -s $line ./$filename
done < ./update-pool.txt
#
cp ./update-pool.txt ./.updated-pool
rm ./update-pool.txt
</pre>

(Drop that code in a txt file, change permissions and run it)

<pre>
[root@box folder]# chmod u+x ./update-pool
[root@box folder]# ./update-pool
</pre>

Here is the end result (all iso’s listed in one folder that libvirt’s virt-manager can handle)

<pre>
[root@box folder]# pwd
/data/path/of/iso/folder
[root@box folder]# ll
total 4
lrwxrwxrwx. 1 root root 43 Apr 22 18:28 debian-506-amd64-CD-1.iso -> ../d/debian/lenny/debian-506-amd64-CD-1.iso
lrwxrwxrwx. 1 root root 47 Apr 22 18:28 debian-6.0.0-amd64-CD-1.iso -> ../d/debian/squeeze/debian-6.0.0-amd64-CD-1.iso
lrwxrwxrwx. 1 root root 48 Apr 22 18:28 debian-6.0.0-amd64-DVD-1.iso -> ../d/debian/squeeze/debian-6.0.0-amd64-DVD-1.iso
lrwxrwxrwx. 1 root root 46 Apr 22 18:28 debian-6.0.0-i386-CD-1.iso -> ../d/debian/squeeze/debian-6.0.0-i386-CD-1.iso
lrwxrwxrwx. 1 root root 47 Apr 22 18:28 debian-6.0.0-i386-DVD-1.iso -> ../d/debian/squeeze/debian-6.0.0-i386-DVD-1.iso
lrwxrwxrwx. 1 root root 52 Apr 22 18:28 Fedora-14-i686-Live-Desktop.iso -> ../f/fedora/laughlin/Fedora-14-i686-Live-Desktop.iso
lrwxrwxrwx. 1 root root 49 Apr 22 18:28 Fedora-14-i686-Live-XFCE.iso -> ../f/fedora/laughlin/Fedora-14-i686-Live-XFCE.iso
lrwxrwxrwx. 1 root root 54 Apr 22 18:28 Fedora-14-x86_64-Live-Desktop.iso -> ../f/fedora/laughlin/Fedora-14-x86_64-Live-Desktop.iso
lrwxrwxrwx. 1 root root 51 Apr 22 18:28 Fedora-14-x86_64-Live-XFCE.iso -> ../f/fedora/laughlin/Fedora-14-x86_64-Live-XFCE.iso
lrwxrwxrwx. 1 root root 49 Apr 22 18:28 FreeBSD-8.2-RELEASE-amd64-livefs.iso -> ../f/freebsd/FreeBSD-8.2-RELEASE-amd64-livefs.iso
lrwxrwxrwx. 1 root root 37 Apr 22 18:28 meego-netbook-ia32-1.1.img -> ../m/meego/meego-netbook-ia32-1.1.img
lrwxrwxrwx. 1 root root 45 Apr 22 18:28 moblin-2.1-final-20091103-002.img -> ../m/moblin/moblin-2.1-final-20091103-002.img
lrwxrwxrwx. 1 root root 24 Apr 22 18:28 Protech-ONE-x86.iso -> ../p/Protech-ONE-x86.iso
lrwxrwxrwx. 1 root root 35 Apr 22 18:28 slackware-13.1-install-dvd.iso -> ../s/slackware-13.1-install-dvd.iso
lrwxrwxrwx. 1 root root 42 Apr 22 18:28 ubuntu-10.10-desktop-amd64.iso -> ../u/ubuntu/ubuntu-10.10-desktop-amd64.iso
lrwxrwxrwx. 1 root root 41 Apr 22 18:28 ubuntu-10.10-desktop-i386.iso -> ../u/ubuntu/ubuntu-10.10-desktop-i386.iso
-rwxr–r–. 1 root root 341 Apr 22 18:32 update-pool
</pre>

Now, you can have the virt-manager storage pool point to “/data/path/of/iso/folder” (the place with your symlinks) and these will be picked up as if they were all actually in the same folder.

So to recap, virt-manager’s pool feature is awesome, but it doesn’t like to recursively scan the folders you place your iso’s in. If you are like me and are rather OCD like, having an alphabetized list/organized file structure of your iso’s is a must. Having others place nicely with this structure it required.
