{{{
   "title" : "Goodbye Wordpress, hello Poet!",
   "tags" : [ "node.js", "poet" ],
   "category" : "Node.js",
   "date" : "8-09-2013",
   "description" : "Goodbye Wordpress, hello Poet!"
}}}

> [Poet](http://jsantell.github.io/poet/) is a blog generator in [node.js](http://nodejs.org/) to generate routing, render markdown/jade/whatever posts, and get a blog up and running fast. Poet may not make you blog-famous, and it may give you one less excuse for not having a blog, but just imagine the insane hipster cred you get for having node power your blog. "Cool blog, is this Wordpress or something?" your square friend asks. "Nah dude, this is in node," you respond, while skateboarding off into the sunset, doing mad flips and stuff. Little do they know you just used Poet's autoroute generation to get your content in, like, seconds, up on that internet. -- [source](http://jsantell.github.io/poet/)

So here I am minding my own business when a thought occurred:

> What if I could structure my blogs posts as text files and keep track of their changes with, ... git, or something.

Thus the hunt began. There are quite a few blogging engines that can generate flat files and use these as blog entries.

+ [Wheat](https://github.com/creationix/wheat)
+ [Blacksmith](https://github.com/flatiron/blacksmith)
+ [Poet](http://jsantell.github.io/poet/)

Wheat does not appear to have been updated in quite some time, however that should not stop you from trying it out or maybe even forking your repo from it.

Blacksmith's updates are bit more recent and it has awesome documentation.

After playing around with all of these I landed on Poet. It's pretty easy to install. I went the lazy route and `git clone`'d the repo:

```
git clone https://github.com/jsantell/poet.git
```

... And started picking it apart. 

More informaton this can be found [here](https://github.com/jsantell/poet). It has excellent documentation. The post files are written in '[markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)' language and rendered as html.

This.is.awesome.

All posts for this blog can now be found at [Bitbucket](http://git.splodycode.net/blog-posts/src).
