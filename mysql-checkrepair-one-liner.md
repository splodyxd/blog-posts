{{{
  "title" : "Mysql Check/Repair (one-liner)",
  "tags" : [ "bash", "mysql", "mysqlcheck" ],
  "category" : "One-liners",
  "date" : "9-7-2011",
  "description" : "Repairing MySQL databases"
}}}

So you did something wrong and you have 100+ databases that need to be repaired. That would go something like this:

<pre>
[root@server ~]# mysql -e 'show databases;'
+--------------------+
| Database           |
+--------------------+
| database1          |
| database2          |
| .........          |
| database276        |
| .........          |
+--------------------+

[root@server ~]# mysqlcheck -r database1
...
[root@server ~]# mysqlcheck -r database236
</pre>

No fun, right? Want to do them all at once, huh? Ready for the punch line, eh?

**MySql Check all databases**

<pre>
for i in `mysql -e 'show databases;' | tail -n +2`;do mysqlcheck -r $i;done;
</pre>

The key part is the “mysql -e ‘show databases” — in the code above, it’s piped to ‘tail’ in order to remove the MySql output formatting. However, you can do many things with it, like checking just one user’s databases:

<pre>
mysql -e 'show databases;' | grep 'user\_' | tail -n +2
</pre>

Checking only a certain type:

<pre>
mysql -e 'show databases;' | grep '\_wordpress*' | tail -n +2
</pre>

<pre>
mysql -e 'show databases;' | grep 'wrdp' | tail -n +2
</pre>

As long as your mysql statement returns results, it will please the for loop and cause it to run

grep + tail = use your imagination.

**Update**

Recently, I ran into a snippet about mysql output formatting - [CommandlineFu](http://www.commandlinefu.com/commands/view/1641/raw-mysql-output-to-use-in-pipes) (I guess reading the man pages would have worked too :p).

All of the above can work without the use of tail (or awk) by using some extra flags:

<pre>
-N removes header
-s removes separator chars
-r raw output
</pre>

<pre>
mysql -Nsre ‘show databases;’ | grep ‘user’
mysql -Nsre ‘show databases;’ | grep ‘wordpress’
</pre>
