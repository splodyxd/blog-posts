{{{
  "title" : "Desktop Screenshot (Debian/Xfce4)",
  "tags" : [ "debian", "linux", "desktop", "xfce4" ],
  "category" : "Desktop",
  "date" : "10-24-2012",
  "description" : "The base for this build is Debian 6 (64-bit). Debian was choosen because it’s rock solid and it works. The Debian distro has an extensive package selection."
}}}

**The Linux desktop, a thing of beauty.**

<img src='http://dev.blog.splodycode.net/img/posts/desktop-screenshot-debian-xfce4/screenshot-done.png' class='center' alt="" width="480" height="126" />

The base for this build is Debian 6 (64-bit). Debian was choosen because it’s rock solid and it works. The Debian distro has an extensive package selection.

> " Much of why Debian is perhaps the best Linux Distribution (and in fact, the best Unix) comes from the core of Debian being its package management. Everything in Debian – every application, every component – everything – is built into a package, and then that package is installed onto your system (either by the Installer, or by you).

> There are over 25 thousand software packages available for Debian – everything from the Linux kernel to games " — http://wiki.debian.org/Apt

Just what can you do with a few of those 25 thousand packages? You can start by installing the following:

+ **Debian 6**: An operating system and a distribution of Free Software – [Link](http://www.debian.org/)
+ **Xfce 4**: A lightweight desktop environment for UNIX-like operating systems – [Link](http://www.xfce.org/)
+ **Nightingale**: A free and powerfull music player for Linux, Windows and Mac – [Link](http://getnightingale.com/)
+ **Xfce4-terminal**: Terminal emulator part of the xfce package – [Link](http://docs.xfce.org/apps/terminal/start)
+ **Iceweasel**: A free software rebranding of the Mozilla Firefox web browser distributed by the GNU Project – [Link](http://www.geticeweasel.org/)
+ **Opera**: An alternative web browser – [Link](http://www.opera.com/)
+ **Geany**: A text editor using the GTK2 toolkit with basic features of an integrated development environment – [Link](http://www.geany.org/)
+ **Thunar**: A new modern file manager for the Xfce Desktop Environment – [Link](http://xfce.org/)
+ **Filezilla**: Open Source (GNU/GPL) FTP client for Windows, Mac OS X and GNU/Linux – [Link](https://filezilla-project.org/)

<img src='http://dev.blog.splodycode.net/img/posts/desktop-screenshot-debian-xfce4/Selection_025.png' class='center'/>

Icons: [AllBlack](http://gnome-look.org/content/show.php/ALLBLACK?content=70630)

Panel/TaskBar: [Tint2](http://code.google.com/p/tint2/)

Theme: [BlackWhite](http://gnome-look.org/content/show.php/BlackWhite?content=68803)

Wallpaper: [Watchmen](http://wallbase.cc/search)

Window manager theme: [Prelude 4](http://xfce-look.org/content/show.php/prelude-4?content=69300)
