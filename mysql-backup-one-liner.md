{{{
  "title" : "MySql Backup (one-liner)",
  "tags" : [ "bash", "mysql", "one-liner" ],
  "category" : "One-liners",
  "date" : "10-16-2011",
  "description" : "Backing up MySQL databases"
}}}

Yes, it can be done.

**One-liner to backup mysql databases**

<pre>
for i in `mysql -e 'show databases;' | tail -n +2`;do mysqldump $i > ./$i.sql;done;tar cvzf dbase-backups.`date +%s`.tar.gz ./*.sql;
</pre>

Now, what is backed up will depend on what user you are running the script as. Often, servers are configured to where normal users are jailed (locked) into their own environment and are not able to see other users or user databases. If this is the case, you will have to run this as root, which would allow you to see/access all the databases.
