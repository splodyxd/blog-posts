{{{
  "title" : "Riak on Debian",
  "tags" : [ "basho", "riak", "nosql", "debian" ],
  "category" : [ "Riak", "Software"],
  "date" : "1-30-2013",
  "description" : "Riak is an open source, highly scalable, fault-tolerant distributed database"
}}}

Riak is an open source, highly scalable, fault-tolerant distributed database – [Basho (Riak)](http://basho.com/riak/)

I have this friend that points and laughs at me when he catches me using “caveman” technology [he doesn't really point and laugh, but I know he is on the inside]. On one occasion, I was discussing with him a project I wanted to tackle: I had invisioned a simple pastebin-like script that would allow me to note my day-to-day internet findings. My list included: php, mysql, a crap-ton of [bash] cgi scripting and lots of late nights trying to get all that to work together. So I gave him my plan, listed the software I would use and he begins his ‘laughter’. “MySQL will present a lot of over head and why would you use bash?”. A few nights later he was giving me far superior mysql alternatives. The one that I liked the most was Riak.

> " Riak is an open source, highly scalable, fault-tolerant distributed database. Different than traditional relational databases and other “NoSQL” non-relational databases — Riak offers a number of unique benefits "

Now me being stuck in my ways could not imagine how this new guy could even dare compete with MySQL. Then I tried it. Some key points that really sold me:

1. **Handling data is simple**: *Buckets and keys are the only way to organize data inside of Riak. Data is stored and referenced by bucket/key pairs. Each key is attached to a unique value that can be any data type* – [source](http://docs.basho.com/riak/1.3.0/references/appendices/concepts/#Buckets-Keys-and-Values)

2. **You can cluster nodes**: *All nodes in a Riak cluster are equal. Each node is fully capable of serving any client request. This is possible due to the way Riak uses consistent hashing to distribute data around the cluster* – [source](http://docs.basho.com/riak/1.3.0/references/appendices/concepts/#Clustering)

3. **It’s childs play to retrieve data**: *Riak uses a REST API for one of the two ways you can access data in Riak. Storage operations use HTTP PUTs or POSTs and fetches use HTTP GETs* – [source](http://docs.basho.com/riak/1.3.0/references/appendices/concepts/#The-Riak-APIs)

So in no time, I had a script set up to install Riak. My target machine would be (of course) a debian box:

<pre>
root@beta:~# uname -m
i686
</pre>

While you can install Riak from source [ instructions on how to install from source ] I chose to install the precompiled binary. This requires a few extra packages:

<pre
Depends: adduser, logrotate, libc6 (>= 2.4), libgcc1 (>= 1:4.1.1), libncurses5 (>= 5.6+20071006-3), libssl0.9.8 (>= 0.9.8k-1), libstdc++6 (>= 4.1.1), sudo
</pre>

These can be install via apt-get:

<pre>
apt-get install adduser logrotate libc6 libgcc1 libncurses5 libssl0.9.8 libstdc++6 sudo
</pre>

After those have been installed you can proceed with installing Riak:

<pre>   
wget http://downloads.basho.com.s3-website-us-east-1.amazonaws.com/riak/1.2/1.2.1/ubuntu/lucid/riak_1.2.1-1_i386.deb -O /tmp/riak_1.2.1-1_i386.deb
dpkg -i /tmp/riak_1.2.1-1_i386.deb
</pre>

If all goes well, you should have Riak installed. Now for the magic. Riak’s Post Installation Notes will have you up and running and will show you some neat tricks. I highly recommend reading the Riak Fast Track, it will help you get your feet wet and show you some of Riaks cool features.

tl;dr – Ditch MySQL, say hello to Riak.
